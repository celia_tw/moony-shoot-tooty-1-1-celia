using UnityEngine;
using System.Collections;

public class MoveConstantly : MonoBehaviour
{
    // used to mark non-public fields as serializable.
    // To serialize an object means to convert its state to a byte stream so that the byte stream can be reverted back into a copy of the object
    [SerializeField]
    private float acceleration = 75f;
    [SerializeField]
    private float initialVelocity = 2f;

    [SerializeField]
    private Vector2 movementDirection = Vector2.up;

    private Rigidbody2D ourRigidbody;

    void Start()
    {
        ourRigidbody = GetComponent<Rigidbody2D>();
        //use variable(movementDireciton) instead of Vector2*up or down, make it flexible
        ourRigidbody.velocity = movementDirection * initialVelocity;
    }


    void Update()
    {
        Vector2 forceToAdd = movementDirection * acceleration * Time.deltaTime;

        ourRigidbody.AddForce(forceToAdd);
    }
}
