﻿using UnityEngine;
using System.Collections;

public class PlayerMovementScript : MonoBehaviour
{

    [SerializeField]
    private float speed = 5000f;
    private Rigidbody2D rb;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    /// <summary>
    /// Accelerate adds an acceleration to the rigidbody component based on the given horizontalInput param.
    /// </summary>
    /// <param name="horizontalInput">horizontalInput should be a value between -1 and 1, given by the player's horizontal input axis</param>
    //horizontalInput is a cotainer for data
    //inject data from outside(from PlayerInput)
    public void Accelerate(float horizontalInput)
    {
        Vector2 forceToAdd = Vector2.right * horizontalInput * speed * Time.deltaTime;
        rb.AddForce(forceToAdd);
    }
}
