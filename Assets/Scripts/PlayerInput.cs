using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{// reference to the PlayerMovementScript
    private PlayerMovementScript playerMovementScript;

    private ShootingScript shootingScript;

    void Start()
    {
        playerMovementScript = GetComponent<PlayerMovementScript>();
        shootingScript = GetComponent<ShootingScript>();
    }

 
    void Update()
    {//store what the play pressing on the horizontal axis into horizontalInput
        float horizontalInput = Input.GetAxis("Horizontal");

        if (horizontalInput != 0.0f)
        {
            //if playerMovementScript does exist
            if (playerMovementScript != null)
            {
                 // pass over our horizontal input
                playerMovementScript.Accelerate(horizontalInput);
            }
        }

        // if shootingScript does exist
        if (shootingScript != null) 
        { 
            //if I'm pressing shoot
          if (Input.GetButton("Fire1"))
            {
                //tell shootingScript to shoot
                shootingScript.Shoot();
             }
        }
    }
}
