﻿using UnityEngine;
using System.Collections;

/// <summary>
/// ShootingScript encapsulates all shooting data and mechanics for the Space Shooter game.
/// Created by Celia
/// </summary>
public class ShootingScript : MonoBehaviour
{

    [SerializeField]
    private GameObject bullet;

    private float lastFiredTime = 0f;

    [SerializeField]
    private float fireDelay = 1f;

    private float bulletOffset = 2f;

    void Start()
    {
        // Do some math to perfectly spawn bullets in front of us
        bulletOffset = GetComponent<Renderer>().bounds.size.y / 2 // Half of our size
            + bullet.GetComponent<Renderer>().bounds.size.y / 2; // Plus half of the bullet size
    }

   /// <summary>
   /// Shoot handles all shooting functionality for the ShootingScript. When this function is called, it
   /// spawns a bullet and will update the state of the ShootingScript to reflect the last time it was fired.
   /// </summary>
   public void Shoot()
    {
            float currentTime = Time.time;

            // Have a delay so we don't shoot too many bullets
            if (currentTime - lastFiredTime > fireDelay)
            {
                Vector2 spawnPosition = new Vector2(transform.position.x, transform.position.y + bulletOffset);

                Instantiate(bullet, spawnPosition, transform.rotation);

                lastFiredTime = currentTime;
            }

       
    }


    public float SampleMethod(int number) {
        return number;
    }

}
