﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum TagListType
{
    Blacklist,Whitelist
}

public class DestroyedOnCollision : MonoBehaviour
{

    [SerializeField]
    private TagListType tagListType = TagListType.Blacklist;

    [SerializeField]
    private List<string> tags;

    void OnTriggerEnter2D(Collider2D other)
    {
        bool tagInList = tags.Contains(other.gameObject.tag);

        if (tagListType == TagListType.Blacklist && tagInList)
        {
            Destroy(gameObject);
        }
        else if (tagListType == TagListType.Whitelist  && !tagInList)
        {
            Destroy(gameObject);
        }
        else
        {

        }
    }

    void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
